<!DOCTYPE html>
<html lang="en">
<head>
    <title>Dashboard</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <a href="{{url('admin/logout')}}" class="btn btn-success" style="float: right;margin-top: 25px;">Logout</a>
    <h1>Available Posts</h1>
    <form class="form-inline">
        <div class="form-group">
            <label>Location:</label>
            <input type="text" class="form-control" name="location" placeholder="Location" value="{{request('location')}}">
        </div>
        <div class="form-group">
            <label>Date:</label>
            <input type="text" class="form-control" name="date" placeholder="yyyy-mm-dd" value="{{request('date')}}">
        </div>
        <button type="submit" class="btn btn-dark">Search</button>
        <a href="{{url('admin/dashboard')}}" class="btn btn-danger">Reset</a>
    </form>
    <br>
    <table class="table table-hover">
        <thead>
        <tr>
            <th>Lat</th>
            <th>Lon</th>
            <th>Location</th>
            <th>Details</th>
            <th>Created On</th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <td colspan="100"><strong>Total {{isset($posts)&&$posts->count()?$posts->count():0}}  posts</strong></td>
        </tr>
        </tfoot>
        <tbody>
        @if(isset($posts) and $posts->count())
            @foreach($posts as $post)
                <tr>
                    <td>{{$post->lat}}</td>
                    <td>{{$post->lon}}</td>
                    <td>{{$post->location_name}}</td>
                    <td>{{$post->details}}</td>
                    <td>{{date('Y-m-d',strtotime($post->created_at))}}</td>
                </tr>
                @endforeach
            @else
            <tr>
                <td colspan="100" class="text-center">No Data Found</td>
            </tr>
            @endif
        </tbody>
    </table>
</div>

</body>
</html>
