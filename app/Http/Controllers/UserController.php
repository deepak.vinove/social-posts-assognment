<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\User;
use Illuminate\Support\Facades\Auth;

/**
 * Represent a user basic activities
 *
 * Class UserController
 * @package App\Http\Controllers
 */
class UserController extends Controller
{
    /**
     * Register a new user into the application
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function register(Request $request)
    {
        try {
            /**
             * ---------Request parameter validation code starts--------
             */
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'username' => 'required',
                'email' => 'required|email|unique:users,email',
                'password' => 'required|min:6|max:15',
            ]);
            if ($validator->fails()) {
                return response()->json(
                    [
                        'code' => 401,
                        'message' => 'Invalid parameters',
                        'data' => $validator->errors()
                    ], 401);
            }
            /**
             * --------Request validation code ends
             */

            //********* creating payload for new user to insert into DB
            $userData = [
                'name' => $request->name,
                'username' => $request->username,
                'email' => $request->email,
                'password' => bcrypt($request->password)
            ];
            $user = User::create($userData);
            //********* getting the token from the newly register user
            $userData['token'] = $user->createToken('social')->accessToken;
            //********  Removing the password from the payload to pass the payload as response
            unset($userData['password']);

            return response()->json(
                [
                    'code' => 200,
                    'message' => 'Register successfully',
                    'message' => $userData,
                ],
                200);
        } catch (\Exception $exception) {
            return response()->json(
                [
                    'code' => 500,
                    'message' => 'server error',
                    'data' => []
                ], 500);
        }
    }

    /**
     * Logged in a user into the application
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        try {
            /**
             * ---------Request parameter validation code starts--------
             */
            $validator = Validator::make($request->all(), [
                'email' => 'required',
                'password' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(
                    [
                        'code' => 401,
                        'message' => 'Invalid parameters',
                        'data' => $validator->errors()
                    ], 401);
            }
            /**
             * --------Request validation code ends
             */
            $credentials = [
                'email' => $request->email,
                'password' => $request->password
            ];

            //  ************** Attempting login
            if (Auth::attempt($credentials)) {
                $user = Auth::user();
                //  ********* generating token
                $success['token'] = $user->createToken('social')->accessToken;
                return response()->json(
                    [
                        'code' => 200,
                        'message' => 'Login success',
                        'data' => $success,
                    ], 200);
            } else {
                return response()->json(
                    [
                        'code' => 401,
                        'message' => 'Unauthorize user access',
                        'data' => [],
                    ], 401);
            }
        } catch (\Exception $exception) {
            return response()->json(
                [
                    'code' => 500,
                    'message' => 'server error',
                    'data' => []
                ], 500);
        }
    }
    /**
     * Logout a user from the application
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        try {
            if (Auth::check()) {
                $user = Auth::user()->token();
                $user->revoke();
            }
            return response()->json(
                [
                    'code' => 200,
                    'message' => 'Logout successfully',
                    'data' => []
                ], 200);
        } catch (\Exception $exception) {
            dd($exception->getMessage());
            return response()->json(
                [
                    'code' => 500,
                    'message' => 'server error',
                    'data' => []
                ], 500);
        }

    }
}
