<?php

namespace App\Http\Controllers;

use App\Posts;
use App\TrandingPost;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

/**
 * Represents users posts
 *
 * Class PostsController
 * @package App\Http\Controllers
 */
class PostsController extends Controller
{
    /**
     * Create a new post for a user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function addPost(Request $request)
    {
        try {
            /**
             * ---------Request parameter validation code starts--------
             */
            $validator = Validator::make($request->all(), [
                'lat' => 'required',
                'lon' => 'required',
                'details' => 'required',
                'location_name' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(
                    [
                        'code' => 401,
                        'message' => 'Invalid parameters',
                        'data' => $validator->errors()
                    ], 401);
            }
            /**
             * --------Request validation code ends
             */

            //  ****** inserting new post in DB
            $post = new Posts();
            $post->user_id = auth()->user()->id;
            $post->lat = $request->lat;
            $post->lon = $request->lon;
            $post->location_name = $request->location_name;
            $post->details = $request->details;
            $post->save();
            return response()->json(
                [
                    'code' => 200,
                    'message' => 'post created successfully',
                    'data' => []
                ], 200);
        } catch (\Exception $exception) {
            return response()->json(
                [
                    'code' => 500,
                    'message' => 'server error',
                    'data' => []
                ], 500);
        }
    }

    /**
     * Fetch a user's posts
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function viewPost(Request $request)
    {
        try {
            //  ****** inserting new post in DB
            $selectFields = 'lat,lon,location_name,details';
            if (!empty($request->lat) && !empty($request->lon) && !empty($request->distance)) {
                $selectFields .= ',111.111 *
            DEGREES(ACOS(LEAST(COS(RADIANS(' . $request->lat . '))
            * COS(RADIANS(lat))
            * COS(RADIANS(' . $request->lon . ' - lon))
            + SIN(RADIANS(' . $request->lat . '))
            * SIN(RADIANS(lat)), 1.0))) AS distance_in_km';
            }
            $post = Posts::selectRaw($selectFields);
            //  if trending post filter apply then get the trending posts only
            if(!empty($request->trending)){
                $post->join('trending_posts','trending_posts.post_id','=','posts.id');
                $post->whereRaw('trending_posts.viewed_at >="'.date('Y-m-d H:i:s',strtotime('-6 hours')).'"');
            }
            if (isset($request->post_id) && !empty($request->post_id)) {
                $post = $post->where('posts.id', $request->post_id);
            }
            //  get only logged in user posts
            $post->where('user_id', auth()->user()->id);
            //  filtering based on distance in KM
            if (!empty($request->lat) && !empty($request->lon) && !empty($request->distance)) {
                $post->having('distance_in_km', '<=', $request->distance);
            }
            //$post->groupBy('posts.id');
            $post = $post->groupBy('posts.id')->get();
            //  if post detail view by user then update this view in trending posts table
            if (isset($request->post_id) && !empty($request->post_id)) {
                $trendingPost = new TrandingPost();
                $trendingPost->post_id = $request->post_id;
                $trendingPost->viewed_at=date('Y-m-d H:i:s');
                $trendingPost->save();
            }
            return response()->json(
                [
                    'code' => 200,
                    'message' => 'Post fetch successfully',
                    'data' => $post
                ], 200);
        } catch (\Exception $exception) {
            dd($exception->getMessage());
            return response()->json(
                [
                    'code' => 500,
                    'message' => 'server error',
                    'data' => []
                ], 500);
        }
    }
}
