<?php

namespace App\Http\Controllers\Web;

use App\Posts;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Admin panel authentication class
 *
 * Class AuthController
 * @package App\Http\Controllers\Web
 */
class AuthController extends Controller
{
    /**
     * Show admin panel login form
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     */
    public function login()
    {
        try {
            //  check already logged in
            if (auth()->check()) {
                return redirect('admin/dashboard');
            }
            return view('admin.login');
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }

    /**
     * Do admin user login
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|string
     */
    public function doLogin(Request $request)
    {
        try {
            //  apply validation
            $this->validate($request,
                [
                    'email' => 'required|email',
                    'password' => 'required|min:6|max:15'
                ]
            );
            //  attempt login
            if (auth()->attempt(['email' => $request->email, 'password' => $request->password])) {
                return redirect('admin/dashboard');
            }
            //  return to login page if attempt failed
            return redirect('admin/login')->with('error', 'Unauthorize login access');
        } catch (\Illuminate\Validate\ValidationException $exception) {
            return view('admin.login');
        }
    }

    /**
     * Takes the admin user to dashboard
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     */
    public function dashboard(Request $request)
    {
        try {
            $posts = Posts::query();
            //  applying filters
            if ($request->location and !empty($request->location)) {
                $posts->where('location_name', 'like', '%' . $request->location . '%');
            }
            if ($request->date and !empty($request->date)) {
                $posts->whereDate('created_at','=', $request->date);
            }
            //  finally get posts
            $posts = $posts->get();
            return view('admin.dashboard', ['posts' => $posts]);
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }

    /**
     * Logout admin user
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|string
     */
    public function logout()
    {
        try {
            //  logging out the user
            auth()->logout();
            return redirect('admin/login');
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }
}
