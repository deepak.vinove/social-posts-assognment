<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrandingPost extends Model
{
    protected $table='trending_posts';
    public $timestamps=false;
}
