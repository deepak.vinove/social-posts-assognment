
# Social Posts

Social posts is an application on which a user can simply create posts.
### This is a simple laravel application.

## Requirements

  - php >= 7.1
  - composer.
  - Mysql >= 5.7/MariaDB >= 10.0

# How to use?
   - Copy project directory to you local server web root then open terminal and go you your project root directory then.
   - Run composer install ```composer install ```
   - Run migration ``` php artisan migrate```
   - You may alternatively need to run laravel passport to functioning well for web tokens.
```php artisan passport:install```
   - 
### API end points
You just need to put your project base url into any http client(Postman) and the append the following endpoints to get the result.
```sh
POST /api/register - To register into the application.
POST /api/login    - To logged into the application.
POST /api/logout   - To logged out from application.
POST /api/post     - To create a post.
GET /api/post      - To view all posts or each post details or trending posts.
if you want to view detail of a single post then
GET /api/post?post_id=YOUR_POST_ID
if you want to view posts under a specific distance from you location then
GET /api/post?lat=YOUR_LATITUDE&lon=YOUR_LONGITUDE&distance=DISTANCE_YOU_WANT
if you want to view trending posts then
GET /api/post?trending=1
```
##  Admin Panel
  - Navigate to your baseurl with admin/login e.g ```localhost/social/public/admin/login ```
  - Make sure one of the user's **is_admin** column set to 1 in database **users** table 
  - Enter login details and that's it.
----

MIT


**Free Software!**

[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)


   [dill]: <https://github.com/joemccann/dillinger>
   [git-repo-url]: <https://github.com/joemccann/dillinger.git>
   [john gruber]: <http://daringfireball.net>
   [df1]: <http://daringfireball.net/projects/markdown/>
   [markdown-it]: <https://github.com/markdown-it/markdown-it>
   [Ace Editor]: <http://ace.ajax.org>
   [node.js]: <http://nodejs.org>
   [Twitter Bootstrap]: <http://twitter.github.com/bootstrap/>
   [jQuery]: <http://jquery.com>
   [@tjholowaychuk]: <http://twitter.com/tjholowaychuk>
   [express]: <http://expressjs.com>
   [AngularJS]: <http://angularjs.org>
   [Gulp]: <http://gulpjs.com>

   [PlDb]: <https://github.com/joemccann/dillinger/tree/master/plugins/dropbox/README.md>
   [PlGh]: <https://github.com/joemccann/dillinger/tree/master/plugins/github/README.md>
   [PlGd]: <https://github.com/joemccann/dillinger/tree/master/plugins/googledrive/README.md>
   [PlOd]: <https://github.com/joemccann/dillinger/tree/master/plugins/onedrive/README.md>
   [PlMe]: <https://github.com/joemccann/dillinger/tree/master/plugins/medium/README.md>
   [PlGa]: <https://github.com/RahulHP/dillinger/blob/master/plugins/googleanalytics/README.md>
