<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'UserController@login');
Route::post('register', 'UserController@register');
Route::group(['middleware' => 'auth:api'], function () {
    Route::get('details', 'UserController@details');
    Route::post('logout', 'UserController@logout');
    Route::post('post', 'PostsController@addPost');
    Route::get('post', 'PostsController@viewPost');
    //Route::get('post/trending','PostsController@trendingPosts');
});
