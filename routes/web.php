<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('admin/login', 'Web\AuthController@login');
Route::post('admin/login', 'Web\AuthController@doLogin');

Route::group(['middleware'=>['admin.login']], function () {
    Route::get('admin/dashboard', 'Web\AuthController@dashboard');
    Route::get('admin/logout', 'Web\AuthController@logout');
});
